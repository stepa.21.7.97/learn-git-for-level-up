<?$APPLICATION->IncludeComponent(
	"bitrix:search.title",
	"corp",
	Array(
		"CATEGORY_0" => array("iblock_1c_catalog"),
		"CATEGORY_0_TITLE" => "ALL",
		"CATEGORY_0_iblock_1c_catalog" => array("24","26"),
		"CATEGORY_0_iblock_aspro_next_catalog" => array("all"),
		"CATEGORY_0_iblock_aspro_next_content" => array("all"),
		"CATEGORY_OTHERS_TITLE" => "OTHER",
		"CHECK_DATES" => "Y",
		"CONTAINER_ID" => "title-search_fixed",
		"CONVERT_CURRENCY" => "N",
		"INPUT_ID" => "title-search-input_fixed",
		"LIST_PROPERTY_CODE" => array(0=>"",1=>"CML2_ARTICLE",2=>"BRAND",3=>"COLOR_REF2",4=>"CML2_LINK",5=>"HAS_DETAIL_PICTURE",6=>"SERIYA_PROIZVODITELYA",7=>"",),
		"NUM_CATEGORIES" => "1",
		"OFFERS_PROPERTY_CODE" => array(0=>"SERIYA_PROIZVODITELYA"),
		"ORDER" => "date",
		"PAGE" => "#SITE_DIR#catalog/",
		"PREVIEW_HEIGHT" => "38",
		"PREVIEW_TRUNCATE_LEN" => "50",
		"PREVIEW_WIDTH" => "38",
		"PRICE_CODE" => array(0=>"BASE",),
		"PRICE_VAT_INCLUDE" => "Y",
		"SHOW_ANOUNCE" => "N",
		"SHOW_INPUT" => "Y",
		"SHOW_INPUT_FIXED" => "Y",
		"SHOW_OTHERS" => "Y",
		"SHOW_PREVIEW" => "Y",
		"TOP_COUNT" => "10",
		"USE_LANGUAGE_GUESS" => "Y"
	),
false,
Array(
	'ACTIVE_COMPONENT' => 'Y'
)
);?>