<h2 class="hide-800">Оборудование для бизнеса</h2>
<div class="index-text-block hide-800">
	<div class="index-text-left">
		<p>
        Железо и Софт - это более 10 000 SKU инженерного оборудования и сложных технических устройств. Ассортимент Железо и Софт позволяет укомплектовать проекты с самыми высокими требованиями к производительности, долговечности оборудования и безопасности сети. Четкий сервис и всегда выгодные условия поставок - то, что позволяет нам успешно конкурировать на российском рынке IT-оборудования и ПО.
		</p>
		<p>
			 Торги на электронных площадках - наше отдельное направление работы. Тендерный отдел Железо и Софт участвует в торгах по ФЗ 233, ФЗ 44.
		</p>
		<p>
			 Наши клиенты: компании различного масштаба, крупные коммерческие и государственные организации.
		</p>
	</div>
	<div class="index-text-right">
 <a target="_blank" href="/products/servery-i-komplektuyushchie/servery/">Серверы</a><a href="/products/servery-i-komplektuyushchie/servery/" target="_blank"></a><br>
 <a href="/products/servery-i-komplektuyushchie/sistemy_khraneniya_dannykh/" target="_blank">С</a><a target="_blank" href="/products/servery-i-komplektuyushchie/sistemy_khraneniya_dannykh/">истемы хранения данных</a><br>
 <a href="/products/programmnoe-obespechenie/informatsionnaya_bezopastnost/" target="_blank">И</a><a target="_blank" href="/products/programmnoe-obespechenie/informatsionnaya_bezopastnost/">нформационная безопасность</a><br>
 <a href="/products/servery-i-komplektuyushchie/programmno_apparatnye_kompleksy/" target="_blank">П</a><a target="_blank" href="/products/servery-i-komplektuyushchie/programmno_apparatnye_kompleksy/">рограммно-аппаратные комплексы</a><br>
 <a href="https://utinet.ru/products/servery-i-komplektuyushchie/opticheskie_biblioteki/" target="_blank">О</a><a target="_blank" href="https://utinet.ru/products/servery-i-komplektuyushchie/opticheskie_biblioteki/">птические библиотеки</a><br>
 <a href="/products/kompyuternye-komplektuyushchie/videokarty/" target="_blank">В</a><a target="_blank" href="/products/kompyuternye-komplektuyushchie/videokarty/">идеокарты</a><br>
 <a target="_blank" href="/products/tv-audio-video/bronirovanie_peregovornykh/">Бронирование переговорных</a><br>
 <a href="https://utinet.ru/products/elektropitanie/ibp_i_stabilizatory_napryazheniya/" target="_blank">И</a><a target="_blank" href="https://utinet.ru/products/elektropitanie/ibp_i_stabilizatory_napryazheniya/">БП и стабилизаторы напряжения</a>
	</div>
 <br>
</div>
<br>