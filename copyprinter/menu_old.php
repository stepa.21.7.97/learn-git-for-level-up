<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<?if (!empty($arResult)):?>
<ul id="horizontal-multilevel-menu">

<?
global $APPLICATION;
$dir = $APPLICATION->GetCurDir();

$previousLevel = 0;
$x=0;
foreach($arResult as $arItem):
   
?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li>
                            <?if ($dir != $arItem["LINK"]):?>   
                            <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>
                            <?else:?>
                            <a class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>                            
                            <?endif;?> 
				<ul>
		<?else:?>
                                    
                                    <li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>>
                                        <?if ($dir != $arItem["LINK"]):?>   
                                            <a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
                                        <?else:?>
                                            <a   class="parent"><?=$arItem["TEXT"]?></a>
                                        <?endif;?> 
                                        
				<ul>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li>
                                        <?if ($dir != $arItem["LINK"]):?>   
                                            <a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>
                                        <?else:?>
                                            <a   class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>
                                        <?endif;?> 
                                </li>
			<?else:?>
                                <?if($arItem["PARAMS"]["class"] == "separator"):?><li class="separator">&nbsp;</li>
                                <?else:?>
				<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>>
                                        <?if ($dir != $arItem["LINK"]):?>   
                                            <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
                                        <?else:?>
                                            <a  ><?=$arItem["TEXT"]?></a>
                                        <?endif;?> 
                                </li>
                                <?endif;?>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li><a href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<div class="menu-clear-left"></div>
<?endif?>