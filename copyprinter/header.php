<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
IncludeTemplateLangFile(__FILE__);

CModule::IncludeModule('iblock');
global $slider_count;

global $APPLICATION;
$dir = $APPLICATION->GetCurDir();

$res = CIBlockElement::GetList(false, array('IBLOCK_ID' => 7, 'ACTIVE' => 'Y'), array('IBLOCK_ID'));
if ($el = $res->Fetch()) {
    $total_slides = $el['CNT'];

    if (isset($_COOKIE['slider_count'])) {
        $slider_count = $_COOKIE['slider_count'];
        $slider_count++;
        if ($slider_count > $total_slides)
            $slider_count = 1;
        setcookie("slider_count", $slider_count, time() + 3600);
    } else
        setcookie("slider_count", "1", time() + 3600);
}


/* НАЧАЛО :: Настройка количества колонок в разделе */
global $page404;
if ($page404)
    $columns = "010";
else
    $columns = $APPLICATION->GetProperty("columns");
switch ($columns) {
    case "111":
        $col1 = 'col1';
        $col3 = 'col3';
        break;
    case "011":
        $col1 = '';
        $col3 = 'col3';
        break;
    case "110":
        $col1 = 'col1';
        $col3 = '';
        break;
    default:
        $col1 = '';
        $col3 = '';
}
/* КОНЕЦ :: Настройка количества колонок в разделе */
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <title><? $APPLICATION->ShowTitle() ?></title>
        <meta name="viewport"  content="initial-scale=1, width=device-width">
        <link rel="shortcut icon" type="image/x-icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico" />
        <link href='https://fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,400italic&amp;subset=latin,cyrillic-ext,cyrillic'
              rel='stylesheet' type='text/css'>
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <![endif]-->

        <? /** CSS * */$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/text.css"); ?>
        <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/template_styles_responsive.css", true); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.min.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery-ui.min.js"); ?>
        <? /** CSS * */$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/fancybox/source/jquery.fancybox.css"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/fancybox/source/jquery.fancybox.pack.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/fancybox/source/init-fancybox.js"); ?>

        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.elevatezoom.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/init-elevatezoom.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/init-zoom.js"); ?>


<? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/tabs/scripts.js"); ?>
        <? /** CSS * */$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/tabs/styles.css"); ?>




        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/init-spoiler.js"); ?>

        <? /** CSS * */$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/jquery.popup.css"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.popup.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/init-popup.js"); ?>

        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/slider/jquery.simpleSlide.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.jcarousel.js"); ?>

        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/init-search.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/adhere.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/init-myscripts.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/scrollingcarousel.2.0.min.js"); ?>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/carusel2.js"); ?>

        <? /** CSS * */$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/mainslider.css"); ?>
        <? if ($APPLICATION->GetCurPage() == "/"): ?>
            <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/mainslider.js"); ?>
<? endif; ?>

<? /** CSS * */$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/offers-for-business.css"); ?>
<? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/offers-for-business.js"); ?>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script><script type="text/javascript">	jQuery(document).ready(function () {
                jQuery('.spoiler-text').hide()
                jQuery('.spoiler').click(function () {
                    jQuery(this).toggleClass("folded").toggleClass("unfolded").next().slideToggle()
                })
    })</script>
        <script type="text/javascript" src="/bitrix/templates/COMPUTERPROF/js/show-hide.js"></script>
        <? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/refreshform.js"); ?>
<? /** SCRIPT * */$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/forms.js"); ?>


<? $APPLICATION->ShowHead(); ?>

    </head>
    <body <?= $APPLICATION->GetProperty("body_style") ?>>
        <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
        <div class="all">
            <div class="header">
              
                    <? if ($dir != "/"): ?>
                        <a class="logo" href="/"></a>
                        <? else: ?>
                        <a class="logo"></a>
                        <? endif; ?>
			<?$APPLICATION->IncludeComponent(
	"reaspekt:reaspekt.geoip", 
	"template1", 
	array(
		"CHANGE_CITY_MANUAL" => "Y",
		"COMPONENT_TEMPLATE" => "template1",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
                    <div class="search-form">
                        <?
                        $APPLICATION->IncludeComponent("bitrix:search.title", ".default", array(
                            "NUM_CATEGORIES" => "3",
                            "TOP_COUNT" => "5",
                            "ORDER" => "rank",
                            "USE_LANGUAGE_GUESS" => "Y",
                            "CHECK_DATES" => "Y",
                            "SHOW_OTHERS" => "Y",
                            "PAGE" => "#SITE_DIR#search/index.php",
                            "CATEGORY_OTHERS_TITLE" => "Прочее",
                            "CATEGORY_0_TITLE" => "Услуги",
                            "CATEGORY_0" => array(
                                0 => "iblock_CP_INFOBLOCKS",
                            ),
                            "CATEGORY_0_iblock_CP_INFOBLOCKS" => array(
                                0 => "22",
                                1 => "23",
                                2 => "31",
                            ),
                            "CATEGORY_1_TITLE" => "О компании",
                            "CATEGORY_1" => array(
                                0 => "main",
                                1 => "iblock_CP_INFOBLOCKS",
                            ),
                            "CATEGORY_1_iblock_CP_INFOBLOCKS" => array(
                                0 => "20",
                                1 => "27",
                                2 => "28",
                                3 => "30",
                            ),
                            "CATEGORY_2_TITLE" => "Пресс-центр",
                            "CATEGORY_2" => array(
                                0 => "iblock_CP_INFOBLOCKS",
                            ),
                            "CATEGORY_2_iblock_CP_INFOBLOCKS" => array(
                                0 => "21",
                                1 => "25",
                                2 => "29",
                            ),
                            "SHOW_INPUT" => "Y",
                            "INPUT_ID" => "title-search-input",
                            "CONTAINER_ID" => "title-search"
                                ), false
                        );
                        ?>
                    </div>
                    <div class="header-phone">
                        <?
                        $APPLICATION->IncludeComponent(
                                "bitrix:main.include", "", Array(
                            "AREA_FILE_SHOW" => "file",
                            "PATH" => "/includes/header_phone.php",
                            "EDIT_TEMPLATE" => ""
                                )
                        );
                        ?>
                    </div>
                    <!--a href="#form-order-call" class="order-call btn show-form pull-right">Заказать обратный звонок</a-->
                    <button class="order-call btn b24-web-form-popup-btn-94 pull-right">Заказать обратный звонок</button>
                    <div class="mainmenu">
                        <?
                        $APPLICATION->IncludeComponent("bitrix:menu", "MAIN_MENU", array(
                            "ROOT_MENU_TYPE" => "top",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MAX_LEVEL" => "2",
                            "CHILD_MENU_TYPE" => "left",
                            "USE_EXT" => "N",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "N"
                                ), false
                        );
                        ?>
                    </div>



                
            </div>
            <div class="slider">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:main.include", "", Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/includes/mainslider.php",
                    "EDIT_TEMPLATE" => ""
                        )
                );
                ?>
            </div>

            <div class="main">
                <div class="screen white-panel">

                    <div class="breadcrumb">
                        <? if ($APPLICATION->GetCurPage() != "/" and ! $page404): ?>
                            <?
                            $APPLICATION->IncludeComponent(
                                    "bitrix:breadcrumb", ".default", array(
                                "START_FROM" => "0",
                                "PATH" => "",
                                "SITE_ID" => "-"
                                    ), false
                            );
                            ?>
                            <? endif; ?>
                    </div>
                                <? SEOPAGE(); ?>

                                <? if ($col1): ?><td class="<?= $col1 ?>">!!!</td><? endif; ?>
                            <td class="maincol">
                                <? if ($page404): ?>
                                    <br><br><h1>Ошибка 404. К сожалению, страница не найдена!</h1>
                                <? else: ?>
                                    <? if (!$APPLICATION->GetProperty("no_h1") or ( $APPLICATION->GetProperty("no_h1") == "show")): ?>
                                        <?
                                        GLOBAL $NEW_H1;
                                        if (isset($NEW_H1)):
                                            ?>
                                            <h1><?= $NEW_H1 ?></h1>
                                        <? else: ?>
                                            <h1><? $APPLICATION->ShowTitle(false) ?></h1>
                                        <? endif; ?>
                                    <? endif; ?>
                                <? endif; ?>
<?
GLOBAL $SEOTEXT;
echo $SEOTEXT;
?>














<!--CONTENTS  -->








<?if ($columns == "011" or $columns == "111"):?>

<?if($APPLICATION->GetCurDir() == "/partner/"):?>
    <script id="bx24_form_inline" data-skip-moving="true">
        (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
            (w[b].forms=w[b].forms||[]).push(arguments[0])};
            if(w[b]['forms']) return;
            var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
            var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://portal.nav-it.ru/bitrix/js/crm/form_loader.js','b24form');

        b24form({"id":"90","lang":"ru","sec":"fgw5w4","type":"inline"});
    </script>
<?else:?>   
    <script id="bx24_form_inline" data-skip-moving="true">
                (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
                    (w[b].forms=w[b].forms||[]).push(arguments[0])};
                    if(w[b]['forms']) return;
                    var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                })(window,document,'https://portal.nav-it.ru/bitrix/js/crm/form_loader.js','b24form');

                b24form({"id":"94","lang":"ru","sec":"y7jfiy","type":"inline"});
            </script>
<?endif;?>
<?/*$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/includes/order_form.php",
                "EDIT_TEMPLATE" => ""
        )
);*/?>
    
<?endif;?>

</div>
            <?if ($col3):?>
<div class="<?=$col3?>">
<?
global $right_col_html;
echo $right_col_html;
?>
<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array("AREA_FILE_SHOW" => "page","AREA_FILE_SUFFIX" => "inc_file_right_col","EDIT_TEMPLATE" => ""	));?>
<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array("AREA_FILE_SHOW" => "sect","AREA_FILE_SUFFIX" => "inc_right_col","AREA_FILE_RECURSIVE" => "Y","EDIT_TEMPLATE" => ""	));?>
<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array("AREA_FILE_SHOW" => "sect","AREA_FILE_SUFFIX" => "inc_right2_col","AREA_FILE_RECURSIVE" => "Y","EDIT_TEMPLATE" => ""	));?>
</div>
                <?endif;?>

<?if ($columns == "010" or $columns == ""):?>
    <div class="wide_inc_area_wrapper_4">
        <?if($APPLICATION->GetCurDir() == "/partner/"):?>
            <script id="bx24_form_inline" data-skip-moving="true">
                (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
                    (w[b].forms=w[b].forms||[]).push(arguments[0])};
                    if(w[b]['forms']) return;
                    var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                })(window,document,'https://portal.nav-it.ru/bitrix/js/crm/form_loader.js','b24form');

                b24form({"id":"90","lang":"ru","sec":"fgw5w4","type":"inline"});
            </script>
        <?else:?> 
            <script id="bx24_form_inline" data-skip-moving="true">
                (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
                    (w[b].forms=w[b].forms||[]).push(arguments[0])};
                    if(w[b]['forms']) return;
                    var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                })(window,document,'https://portal.nav-it.ru/bitrix/js/crm/form_loader.js','b24form');

                b24form({"id":"94","lang":"ru","sec":"y7jfiy","type":"inline"});
            </script>
        <?endif;?>
        <?/*$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/includes/order_form.php",
                        "EDIT_TEMPLATE" => ""
                )
        );*/?>
    </div>        
<?endif;?>

    
    
</div>
</div>  
<div class="wide_inc_area_wrapper_1" style="display:none;">
        <div class="screen">
            
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
"AREA_FILE_SHOW" => "file",
    "PATH" => "/includes/under_content.php",
    "EDIT_TEMPLATE" => ""
),
false,
array(
"ACTIVE_COMPONENT" => "N"
)
);?>        
            <div class="treug1"></div>
            <div class="treug2"></div>
        </div>
    </div>        
    <div class="wide_inc_area_wrapper_2">
        <div class="screen">
            <?
            if ($APPLICATION->GetCurPage() != "/about/clients/"){$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array("AREA_FILE_SHOW" => "file","PATH" => "/includes/under_content_2.php","EDIT_TEMPLATE" => ""),false);}
            ?>            
        </div>        
    </div>        
    <div class="wide_inc_area_wrapper_3">.
        <div class="screen">
            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/includes/under_content_3.php",
        "EDIT_TEMPLATE" => ""
        ),
        false
);?>            
        </div>        
    </div>        

<div class="footer clearfix">
<div class="screen clearfix">
    <? if ($dir != "/"): ?>   
        <a class="logo-white-small" href="/"></a> 
    <? else: ?>
        <a class="logo-white-small"  ></a> 
    <? endif; ?> 



    <div class="social">
    <?$APPLICATION->IncludeComponent(
"bitrix:main.include",
"",
Array(
    "AREA_FILE_SHOW" => "file",
    "PATH" => "/includes/footer_social.php",
    "EDIT_TEMPLATE" => ""
)
);?></div>
    
<div class="footer-menu">
    <?$APPLICATION->IncludeComponent(
"bitrix:menu", 
"FOOTER_MENU", 
array(
    "ROOT_MENU_TYPE" => "top",
    "MENU_CACHE_TYPE" => "N",
    "MENU_CACHE_TIME" => "3600",
    "MENU_CACHE_USE_GROUPS" => "Y",
    "MENU_CACHE_GET_VARS" => array(
    ),
    "MAX_LEVEL" => "1",
    "CHILD_MENU_TYPE" => "left",
    "USE_EXT" => "N",
    "DELAY" => "N",
    "ALLOW_MULTI_SELECT" => "N"
),
false
);?>               
</div>   
    
    
</div> 
<div class="footer2">
<div class="screen">
    <div class="metrika">
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter47539876 = new Ya.Metrika({
                id:47539876,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true,
                trackHash:true
            });
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47539876" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->        </div>
    <div class="copyright">
        <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/includes/footer_copyright.php",
                        "EDIT_TEMPLATE" => ""
                )
        );?>
        
        
        
        
    </div>
    <div class="footer-phone">
        <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/includes/footer_phone.php",
                        "EDIT_TEMPLATE" => ""
                )
        );?>
    </div>
       
    <div class="navigator">
        <a class="link-navigator" href="http://web.nav-it.ru/" rel="nofollow"></a>
        <p>Создание сайта</p>
    </div>
     <div class="bx-composite-banner-conteiner"><div id="bx-composite-banner"></div></div>

        
</div>   
</div>            

</div>            

<?global $APPLICATION;?>
<? if ($APPLICATION->GetCurPage() == "/about/partnership/") {$APPLICATION->IncludeComponent("bitrix:main.include","",Array("AREA_FILE_SHOW" => "file","PATH" => "/includes/form_order_cooperation.php","EDIT_TEMPLATE" => ""));}?>
<? if ($APPLICATION->GetCurPage() == "/about/leadership/") {$APPLICATION->IncludeComponent("bitrix:main.include","",Array("AREA_FILE_SHOW" => "file","PATH" => "/includes/form_write_to_director.php","EDIT_TEMPLATE" => ""));}?>

<?//Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area");?>
<?//$APPLICATION->IncludeComponent("bitrix:main.include","",Array("AREA_FILE_SHOW" => "file","PATH" => "/includes/form_order_callback.php","EDIT_TEMPLATE" => ""));?>
<?//$APPLICATION->IncludeComponent("bitrix:main.include","",Array("AREA_FILE_SHOW" => "file","PATH" => "/includes/form_order_service.php","EDIT_TEMPLATE" => ""));?>
<?//Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area", "");?>
<script id="bx24_form_button" data-skip-moving="true">
(function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u;
    (w[b].forms=w[b].forms||[]).push(arguments[0])};
    if(w[b]['forms']) return;
    var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
})(window,document,'https://portal.nav-it.ru/bitrix/js/crm/form_loader.js','b24form');

b24form({"id":"94","lang":"ru","sec":"y7jfiy","type":"button","click":""});
</script>

    </div>        


<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-63053227-1', 'auto');
ga('send', 'pageview');

</script>

</body>
</html>