<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?> <?
global $USER;
if ($USER->IsAdmin() or 1):
    $APPLICATION->SetAdditionalCSS("/includes/calculator/styles.css");
    $APPLICATION->SetAdditionalCSS("/includes/calculator/jquery-ui.css");
    $APPLICATION->AddHeadScript("/includes/calculator/js.js");
    ?>
<div class="calculator">
	<div class="calculator__wrapper">
		<div class="calculator__title">
			 Расчет стоимости абонентского обслуживания компьютеров&nbsp;
		</div>
		<div class="calculator__row">
			<div class="calculator__left_panel">
				<div class="calculator__contentSlider1_label">
					 Количество компьютеров
				</div>
 <input class="calculator__contentSlider1" type="text" value="">
				<div class="calculator__slider1">
				</div>
			</div>
			<div class="calculator__right_panel">
				<div class="calculator__contentSlider2_label">
					 Количество серверов
				</div>
 <input class="calculator__contentSlider2" type="text" value="">
				<div class="calculator__slider2">
				</div>
			</div>
		</div>
		<div class="calculator__results">
			<div class="calculator__result_label">
				 Стоимость обслуживания:
			</div>
			<div class="calculator__result">
			</div>
		</div>
		<div class="calculator__order-link">
			 <script data-b24-form="click/94/y7jfiy" data-skip-moving="true">
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://portal.nav-it.ru/upload/crm/form/loader_94_y7jfiy.js');
</script> <a href="javascript:void(0)" class="show-form order-link order-service">Заказать</a>
		</div>
	</div>
</div>
<? endif; ?>