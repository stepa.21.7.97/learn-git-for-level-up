<? 
//$page = $APPLICATION->GetCurPage();
//if($page=="/"):?>
<h1>ИТ аутсорсинг в Санкт-Петербурге</h1>
 <?// endif;?>
<div class="standart-news-list-2cols clearfix">
	<table class="tab-standart-news-list">
	<tbody>
	<tr>
		<td class="news-item">
 <a href="/services/it-audit/">
			<div class="PREVIEW_PICTURE">
 <img width="60" alt="code-review.png" src="https://ekb.copyprinter.ru/upload/medialibrary/657/657fe702a3fda74a221fd2187cda66f5.png" height="60" title="code-review.png" class="preview_picture">
			</div>
			<div class="PREVIEW_TEXT">
				<div class="NAME">
					 ИТ-аудит компьютеров и компьютерных сетей
				</div>
			</div>
 </a>
		</td>
		<td class="news-item">
 <a href="/services/nastrojka-setej/">
			<div class="PREVIEW_PICTURE">
 <img width="60" alt="computer (4).png" src="https://ekb.copyprinter.ru/upload/medialibrary/63f/63fba159c045e5ad59d9d1f62dfd0bb4.png" height="60" title="computer (4).png" class="preview_picture">
			</div>
			<div class="PREVIEW_TEXT">
				<div class="NAME">
					 Настройка и обслуживание ИТ-сетей
				</div>
			</div>
 </a>
		</td>
	</tr>
	<tr>
		<td class="news-item">
 <a href="/services/zapravka-kartridzhey/">
			<div class="PREVIEW_PICTURE">
 <img width="60" alt="cartridge (3).png" src="https://ekb.copyprinter.ru/upload/medialibrary/277/277a3528c3b77e00d37afab4c9dfbd4e.png" height="60" title="cartridge (3).png" class="preview_picture">
			</div>
			<div class="PREVIEW_TEXT">
				<div class="NAME">
					 Заправка картриджей
				</div>
			</div>
 </a>
		</td>
		<td class="news-item">
 <a href="/services/sborka-kompjuterov/">
			<div class="PREVIEW_PICTURE">
 <img width="60" alt="pc-tower.png" src="https://ekb.copyprinter.ru/upload/medialibrary/fd8/fd8e7890c0a39dbd55e68d7600be0b1d.png" height="60" title="pc-tower.png" class="preview_picture">
			</div>
			<div class="PREVIEW_TEXT">
				<div class="NAME">
					 Сборка компьютеров на заказ
				</div>
			</div>
 </a>
		</td>
	</tr>
	</tbody>
	</table>
</div>
<p>
	 С 2005 года силами специалистов CopyPrint обеспечивается <b>бесперебойная работа ИТ-инфраструктуры предприятий на территории России</b>.
</p>
<p>
	 Наши клиенты - производственные и торговые компании, а также предприятия сферы услуг, которые решили снять с себя "головную боль" по обеспечению надёжной технической базы.
</p>
<p>
	 Мы берем на себя частичное или полное обслуживание ИТ-систем, обеспечивая бесперебойную работу компьютерных и серверных сетей.&nbsp;
</p>
<p>
	 Мы выстраиваем работу ИТ-инфраструктуры федеральных предприятий и филиальных сетей, делая<b> бизнес заказчика более прозрачным и управляемым</b>.
</p>
<p>
	 Мы ориентированы на долгосрочное сотрудничество. Решая локальную проблему, мы думаем о том, как выстроить работу так, чтобы эта проблема больше не возникала. Постепенно мы оптимизируем работу компании заказчика, <b>стабилизируя работу ИТ-систем и снижая число инцидентов</b>.
</p>
<p>
	 Услуги комплексного ИТ-аутсорсинга включают обслуживание компьютерного и серверного оборудования, оптимизацию ИТ-архитектуры&nbsp;заказчика в гиперконвергентной среде. Мы подчиняем все меры единой цели - сделать так, чтобы у сотрудников <b>не было необходимости задумываться </b>о том, на чём они работают и как устроена информационная система компании. Наша цель - обеспечить возможность сотрудникам <b>выполнять свою работу</b>.
</p>
<p>
 <br>
</p>
<div class="selection">
	<p class="p-selection">
		 Наш результат - это когда пользователи прекращают искать решения ИТ-проблем фокусируются на рабочих операциях
	</p>
</div>
<p>
 <br>
</p>
<h2>Более 15 лет опыта обслуживания серверных систем и клиентских устройств</h2>
 <br>
<div class="quality">
	<div class="quality-item ">
		<div>
 <img width="100" alt="ruble (1).png" src="https://ekb.copyprinter.ru/upload/medialibrary/be0/be0c0ec2bb734950dd39a7b7137097e4.png" height="100" title="ruble (1).png">
		</div>
		<div class="quality-block-text">
			<div class="span">
				 Цена
			</div>
			<div class="quality-item-text">
 <span style="font-size: 10pt;">Стоимость ИТ-аутсорсинга всегда ниже, чем собственного ИТ-специалиста</span>
			</div>
		</div>
	</div>
	<div class="quality-item ">
		<div>
 <img alt="ruble.png" src="https://ekb.copyprinter.ru/upload/medialibrary/915/9152bb2b497b74895cef8b66859c32c8.png" height="100" width="" title="ruble.png">
		</div>
		<div class="quality-block-text">
			<div class="span">
				 Сервис
			</div>
			<div class="quality-item-text">
 <span style="font-size: 10pt;">Выездной системный администратор не болеет и не ходит в отпуск, а горячая линия может работать 24/7/365</span>
			</div>
		</div>
	</div>
	<div class="quality-item ">
		<div>
 <img alt="ruble.png" src="https://ekb.copyprinter.ru/upload/medialibrary/e02/e02d9d300119bf32ffe1915c55b0fe48.png" height="100" width="" title="ruble.png">
		</div>
		<div class="quality-block-text">
			<div class="span">
				 Гарантии
			</div>
			<div class="quality-item-text">
 <span style="font-size: 10pt;">Стоимость, срок реакции на инцидент, время обслуживания и прочие условия прописаны в договоре и соблюдаются нашими сотрудниками</span>
			</div>
		</div>
	</div>
	<div class="quality-item ">
		<div>
 <img alt="ruble.png" src="https://ekb.copyprinter.ru/upload/medialibrary/606/606eedfceeb0354c403b44b89f22a9ab.png" height="100" width="" title="ruble.png">
		</div>
		<div class="quality-block-text">
			<div class="span">
				 Опыт
			</div>
			<div class="quality-item-text">
 <span style="font-size: 10pt; ">Благодаря опыту наших инженеров мы находим простые решения и можем максимально быстро отреагировать на инцидент</span>
			</div>
		</div>
	</div>
</div>
 <br>
<h2>Форматы ИТ-обслуживания</h2>
<p>
</p>
<ul class="list6b">
	<li>ИТ-обслуживание на объекте заказчика в любом регионе России</li>
	<li>Горячая линия технической поддержки по единому номеру</li>
	<li>Разовые работы по устранению аварий и улучшению работы сети</li>
	<li>Построение системы удалённой работы компании</li>
</ul>
 График работы, интенсивность и другие детали определяются индивидуально в момент заключения договора.<br>
<h2>Регионы ИТ-обслуживания </h2>
<p>
	 Услуги ИТ-аутсорсинга предоставляются во всех регионах России.
</p>
<p>
	 Если вы не нашли Ваш город на карте, пишите на <a href="mailto:copyprint.ru">copyprinter.ru</a><br>
	 Возможно, организация ИТ-обслуживания в Вашем городе как раз у нас в планах.
</p>
<p>
	 <?/* $APPLICATION->IncludeComponent("bitrix:news.list", "it-outsoursing-main", array(
    "IBLOCK_TYPE" => "CP_INFOBLOCKS",
    "IBLOCK_ID" => "22",
    "NEWS_COUNT" => "20",
    "USE_SEARCH" => "N",
    "USE_RSS" => "N",
    "USE_RATING" => "N",
    "USE_CATEGORIES" => "N",
    "USE_REVIEW" => "N",
    "USE_FILTER" => "N",
    "SORT_BY1" => "SORT",
    "SORT_ORDER1" => "DESC",
    "SORT_BY2" => "ACTIVE_FROM",
    "SORT_ORDER2" => "DESC",
    "CHECK_DATES" => "Y",
    "SEF_MODE" => "Y",
    "SEF_FOLDER" => "/",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
    "AJAX_OPTION_HISTORY" => "N",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "36000000",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "Y",
    "SET_STATUS_404" => "Y",
    "SET_TITLE" => "Y",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
    "ADD_SECTIONS_CHAIN" => "N",
    "ADD_ELEMENT_CHAIN" => "Y",
    "USE_PERMISSIONS" => "N",
    "PREVIEW_TRUNCATE_LEN" => "",
    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
    "LIST_FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "LIST_PROPERTY_CODE" => array(
        0 => "",
        1 => "",
    ),
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "DISPLAY_NAME" => "Y",
    "META_KEYWORDS" => "KEYWORDS",
    "META_DESCRIPTION" => "DESCRIPTION",
    "BROWSER_TITLE" => "TITLE",
    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
    "DETAIL_FIELD_CODE" => array(
        0 => "",
        1 => "",
    ),
    "DETAIL_PROPERTY_CODE" => array(
        0 => "NEW_H1",
        1 => "ARTICLES",
        2 => "PRICE",
        3 => "",
    ),
    "DETAIL_DISPLAY_TOP_PAGER" => "N",
    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
    "DETAIL_PAGER_TITLE" => "Страница",
    "DETAIL_PAGER_TEMPLATE" => "",
    "DETAIL_PAGER_SHOW_ALL" => "Y",
    "PAGER_TEMPLATE" => "modern",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "Y",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "DISPLAY_DATE" => "Y",
    "DISPLAY_PICTURE" => "Y",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "USE_SHARE" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "SEF_URL_TEMPLATES" => array(
        "news" => "",
        "section" => "",
		"detail" => "#ELEMENT_CODE#/",
    )
        ), false
); */?>
</p>
<div style="margin-bottom: 40px;">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	"map_view_custom",
	Array(
		"COMPONENT_TEMPLATE" => "map_view_custom",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONTROLS" => array(0=>"ZOOM",1=>"MINIMAP",2=>"TYPECONTROL",3=>"SCALELINE",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:58.843193246627486;s:10:\"yandex_lon\";d:81.53368352163697;s:12:\"yandex_scale\";i:3;s:10:\"PLACEMARKS\";a:24:{i:0;a:3:{s:3:\"LON\";d:30.337184740873;s:3:\"LAT\";d:59.873729167333;s:4:\"TEXT\";s:29:\"Санкт-Петербург\";}i:1;a:3:{s:3:\"LON\";d:37.645518258513;s:3:\"LAT\";d:55.714448805849;s:4:\"TEXT\";s:12:\"Москва\";}i:2;a:3:{s:3:\"LON\";d:142.84838130034;s:3:\"LAT\";d:46.742634491294;s:4:\"TEXT\";s:27:\"Южно-Сахалинск\";}i:3;a:3:{s:3:\"LON\";d:20.659964978626;s:3:\"LAT\";d:54.593658096639;s:4:\"TEXT\";s:22:\"Калининград\";}i:4;a:3:{s:3:\"LON\";d:50.091001113227;s:3:\"LAT\";d:53.082550180353;s:4:\"TEXT\";s:12:\"Самара\";}i:5;a:3:{s:3:\"LON\";d:44.140583912284;s:3:\"LAT\";d:56.095392794722;s:4:\"TEXT\";s:29:\"Нижний Новгород\";}i:6;a:3:{s:3:\"LON\";d:39.828588047162;s:3:\"LAT\";d:59.087630272244;s:4:\"TEXT\";s:15:\"Вологда \";}i:7;a:3:{s:3:\"LON\";d:45.409642734662;s:3:\"LAT\";d:54.089367087751;s:4:\"TEXT\";s:14:\"Саранск\";}i:8;a:3:{s:3:\"LON\";d:55.341283359662;s:3:\"LAT\";d:51.575166988409;s:4:\"TEXT\";s:16:\"Оренбург\";}i:9;a:3:{s:3:\"LON\";d:46.156713047162;s:3:\"LAT\";d:51.314283921768;s:4:\"TEXT\";s:14:\"Саратов\";}i:10;a:3:{s:3:\"LON\";d:39.700252371735;s:3:\"LAT\";d:47.01244772794;s:4:\"TEXT\";s:12:\"Ростов\";}i:11;a:3:{s:3:\"LON\";d:39.085017996735;s:3:\"LAT\";d:44.725842808655;s:4:\"TEXT\";s:18:\"Краснодар\";}i:12;a:3:{s:3:\"LON\";d:42.031987985197;s:3:\"LAT\";d:44.944724353309;s:4:\"TEXT\";s:20:\"Ставрополь\";}i:13;a:3:{s:3:\"LON\";d:57.632443184819;s:3:\"LAT\";d:65.918996018334;s:4:\"TEXT\";s:8:\"Ухта\";}i:14;a:3:{s:3:\"LON\";d:60.755540055237;s:3:\"LAT\";d:56.704615554717;s:4:\"TEXT\";s:26:\"Екатеринбург  \";}i:15;a:3:{s:3:\"LON\";d:75.666140956941;s:3:\"LAT\";d:63.043648805964;s:4:\"TEXT\";s:17:\"Ноябрьск \";}i:16;a:3:{s:3:\"LON\";d:85.139226484274;s:3:\"LAT\";d:56.342951313975;s:4:\"TEXT\";s:11:\"Томск \";}i:17;a:3:{s:3:\"LON\";d:73.620090620298;s:3:\"LAT\";d:61.057390825577;s:4:\"TEXT\";s:13:\"Сургут \";}i:18;a:3:{s:3:\"LON\";d:104.47301170663;s:3:\"LAT\";d:52.102581425452;s:4:\"TEXT\";s:14:\"Иркутск\";}i:19;a:3:{s:3:\"LON\";d:104.51695701913;s:3:\"LAT\";d:51.926329379252;s:4:\"TEXT\";s:14:\"Ангарск\";}i:20;a:3:{s:3:\"LON\";d:115.02885202949;s:3:\"LAT\";d:60.633385738858;s:4:\"TEXT\";s:11:\"Ленск \";}i:21;a:3:{s:3:\"LON\";d:124.80589129258;s:3:\"LAT\";d:56.578173161011;s:4:\"TEXT\";s:16:\"Нерюнгри\";}i:22;a:3:{s:3:\"LON\";d:135.52731847061;s:3:\"LAT\";d:48.169719243901;s:4:\"TEXT\";s:20:\"Хабаровск  \";}i:23;a:3:{s:3:\"LON\";d:72.692275733799;s:3:\"LAT\";d:65.455405322937;s:4:\"TEXT\";s:11:\"Надым \";}}}",
		"MAP_HEIGHT" => "400",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(0=>"ENABLE_SCROLL_ZOOM",1=>"ENABLE_DBLCLICK_ZOOM",2=>"ENABLE_DRAGGING",)
	)
);?>
</div>
<h2>Стоимость обслуживания компьютеров и серверов</h2>
<p>
	 Стоимость ИТ-аутсорсинга определяется числом единиц техники, режимом и форматом работы. При расчёте стоимости обслуживания предприятий с более, чем 40 ПК, стоимость всегда рассчитывается индивидуально.
</p>
<p>
	 Ниже калькулятор для первичного расчёта стоимости обслуживания объекта.
</p>
<p>
	 Для точного расчёта стоимости обслуживания пишите на <a href="mailto:copyprinter.ru">copyprinter.ru</a>
</p>
<p>
 <br>
</p>
 <? 
$page = $APPLICATION->GetCurPage();
if($page=="/"):?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/calculator/calculator.php"
	)
);?><?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"COMPONENT_TEMPLATE" => "",
		"EDIT_TEMPLATE" => "",
		"PATH" => "/includes/index_outsourcing.php"
	)
);?>
<p>
</p>
<div class="aktsiya">
	 <!--<div class="ak_zagol">
		 Перевод компании на удаленную работу
	</div>-->
</div>
<div class="ak_button">
	 <!--a href="#order-service" class="show-form order-link-glav order-service">Получить консультацию</a--> <a href="javascript:void(0)" class="b24-web-form-popup-btn-89 order-link-glav order-service">Получить консультацию</a>
</div>
<div class="table-responsive " style="overflow-x: unset;">
</div>
<table class="banners-uslugi table">
<tbody>
<tr>
	<td class="coll1">
		<div style="text-align: center;">
 <img src="/bitrix/images/icons/obsl_komp1.png" alt="">
		</div>
 <a href="/services/prikhodyashchiy-sistemnyy-administrator/">Приходящий системный администратор</a> <a href="/services/udalennyj-sistemnyj-administrator/">Удалённое администрирование</a> <a href="/services/remont-i-obsluzhivanie-noutbukov/">Обслуживание и ремонт ноутбуков</a><a href="/services/razovye-raboty-sla/">Разовые работы SLA</a>
	</td>
	<td class="coll2">
		<div style="text-align: center;">
 <img src="/bitrix/images/icons/obsl_orgtech1.png" alt="">
		</div>
 <a href="/services/obsluzhivanie-printerov/">Обслуживание принтеров</a> <a href="/services/remont-skanerov/">Обслуживание сканеров</a> <a href="/services/obsluzhivanie-orgtekhniki/">Обслуживание оргтехники</a> <a href="/services/zapravka-kartridzhey/">Заправка картриджей</a>
	</td>
	<td class="coll3">
		<div style="text-align: center;">
 <img src="/bitrix/images/icons/it_out1.png" alt="">
		</div>
 <a href="/services/it-konsalting/">IT консалтинг</a> <a href="/services/it-audit/">IT-аудит</a> <a href="/services/sborka-kompjuterov/">Сборка компьютеров</a>
	</td>
</tr>
<tr>
	<td class="coll4 zak-left">
		<div style="text-align: center;">
 <img src="/bitrix/images/icons/obsl_serv1.png" alt="">
		</div>
 <a href="/services/tekhnicheskoe-obsluzhivanie-serverov/">Техническое обслуживание серверов</a> <a href="/services/nastroyka-i-obsluzhivanie-faylovogo-servera/">Обслуживание файлового сервера</a> <a href="/services/obsluzhivanie-pochtovogo-servera/">Обслуживание почтового сервера</a> <a href="/services/nastroyka-servera-1s/">Настройка и обслуживание сервера 1С</a>
	</td>
	<td class="coll5">
		<div style="text-align: center;">
 <img src="/bitrix/images/icons/komp_seti1.png" alt="">
		</div>
 <a href="/services/nastrojka-setej/">Настройка сетей любой сложности</a>
	</td>
	<td class="coll6 zak-right">
		<div style="text-align: center;">
 <img src="/bitrix/images/icons/torg_obor1.png" alt="">
		</div>
 <a href="/services/remont-i-obsluzhivanie-torgovogo-oborudovaniya/">Обслуживание торгового оборудования</a>
	</td>
</tr>
</tbody>
</table>
 <br>
<h2>Абонентское обслуживание компьютеров организаций</h2>
<p>
	 CopyPrint предоставляет качественное и своевременное абонентское обслуживание компьютеров организаций в Санкт-Петербурге. Если у Вас по каким-то причинам нет специалиста по обслуживанию компьютерной техники или другого оборудования, мы готовы поспешить к вам на помощь! Услуги полноценной диагностики, оперативное восстановление неисправностей и просто плановая проверка – все это мы выполняем на высшем уровне. Доверив свою технику нашим опытным и квалифицированным специалистам, вы сможете быть спокойны и уверенны в надежном партнере.
</p>
<div class="citate">
	<h3 class="vrezka2 skrtie">Цены на обслуживание компьютеров</h3>
	<div class="table-responsive skrtie">
 <br>
		<table class="table table-responsive table-bordered nice">
		<tbody>
		<tr class="bckgrnd-gr">
			<th style="vertical-align: middle; text-align: center;">
				 Техническое обслуживание <br>
				 (кол-во ПК)
			</th>
			<th style="vertical-align: middle; text-align: center;">
				 Кол-во мероприятий <br>
				 (выездов)
			</th>
			<th style="vertical-align: middle; text-align: center;">
				 Продолжительность <br>
				 одного мероприятия <br>
				 (в час.)
			</th>
			<th style="vertical-align: middle; text-align: center;">
				 Стоимость обслуживания <br>
				 в месяц
			</th>
			<th style="vertical-align: middle; text-align: center;">
				 ИТ - Аудит
			</th>
		</tr>
		<tr>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 1-3
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 1
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 2 400
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 1 500
			</td>
		</tr>
		<tr>
			<td style="vertical-align: middle; text-align: center;">
				 1
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
		</tr>
		<tr style="background: #d6d9da;">
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 4-6
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 4 800
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 2 200
			</td>
		</tr>
		<tr style="background: #d6d9da;">
			<td style="vertical-align: middle; text-align: center;">
				 1
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 7-10
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 5 900
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 3 000
			</td>
		</tr>
		<tr>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
		</tr>
		<tr style="background: #d6d9da;">
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 11-13
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 9 600
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 3 600
			</td>
		</tr>
		<tr style="background: #d6d9da;">
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 14-16
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 3
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 11 900
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 4 400
			</td>
		</tr>
		<tr>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
		</tr>
		<tr style="background: #d6d9da;">
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 17-20
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 14 400
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 5 300
			</td>
		</tr>
		<tr style="background: #d6d9da;">
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 21-30
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 5
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 16 800
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 6 600
			</td>
		</tr>
		<tr>
			<td style="vertical-align: middle; text-align: center;">
				 2
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
		</tr>
		<tr style="background: #d6d9da;">
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 31-40
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 6
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 21 600
			</td>
			<td rowspan="2" style="vertical-align: middle; text-align: center;">
				 7 600
			</td>
		</tr>
		<tr style="background: #d6d9da;">
			<td style="vertical-align: middle; text-align: center;">
				 3
			</td>
			<td style="vertical-align: middle; text-align: center;">
				 4
			</td>
		</tr>
		</tbody>
		</table>
	</div>
 <br>
	<h3>ИТ-аутсорсинг с CopyPrint</h3>
	<p>
		 От возникновения сбоев и поломок компьютеров, серверов или другой техники не застрахована ни одна организация, и даже самая надежная система или устройство однажды выходит из строя. Грамотное и оперативное реагирование на проблему – это наша прямая обязанность. Мы работаем на основе договора, который составляется на определенные оговоренные сроки. Предотвратить неполадки и не допустить выход из строя оборудования поможет диагностика, регулярные проверки состояния компьютерной техники, которые производят наши высококвалифицированные IT-специалисты.
	</p>
	<p>
 <br>
	</p>
	<div style="text-align: center;">
 <img width="75%" alt="ИТ-аутсорсинг.png" src="https://ekb.copyprinter.ru/upload/medialibrary/cac/cacd12e60da14e1444a42e734974b632.jpg" height="" title="ИТ-аутсорсинг.png"><br>
	</div>
	<div class="nice-panel">
		<p>
 <br>
		</p>
		<h3>Услуги CopyPrint</h3>
		<p>
			 Мы позаботимся о качественном абонентском обслуживании компьютеров и оптимизации сетей, проведем ИТ-консультирование и оперативно выполним срочное восстановление данных, при необходимости произведем установку программного обеспечения на компьютеры или сервера вашей организации.
		</p>
		<ul class="list6b">
			<li>Создание ИТ-инфраструктуры для новой компании или обновление старой базы для давно существующей организации, закупка и поставка компьютерного оборудования;</li>
			<li>Полная диагностика и настройка компьютерной техники;</li>
			<li>Оптимизация функционирования компьютеров и ИТ-услуги по настройке серверов;</li>
			<li>Сервисное обслуживание вышедшего из строя оборудования, замена комплектующих и восстановление утерянных данных, резервное копирование информации и обеспечение надежного хранения данных;</li>
			<li>Создание надежной защиты систем от взломов и хакерских атак;</li>
			<li>Администрирование, настройка и наладка сетей (локальных, Интернет, Wi-Fi);</li>
			<li>Антивирусная проверка, удаление вирусов и вредоносных программ;</li>
			<li>Поддержка безупречной работы оборудования (принтеров, сканеров, модемов);</li>
			<li>Подбор программной базы, установка и настройка офисных программ;</li>
		</ul>
	</div>
	<blockquote>
		 Занимаясь абонентским обслуживанием компьютеров организаций, мы заслужили безупречную репутацию. На рынке IT-аутсорсинга компания находится уже десять лет (основана в 2005 году).
	</blockquote>
	<p>
		 Наша гарантия качества – это большое число постоянных клиентов, количество которых увеличивается с каждым днем. На сегодня мы сотрудничаем с более 100 компаний и всегда рады пополнению клиентской базы, которое служит главным комплиментом качеству нашей работы.
	</p>
	<hr>
	 <?/*$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "news_on_mainpage",
    Array(
        "IBLOCK_TYPE" => "CP_INFOBLOCKS",
        "IBLOCK_ID" => "22",
        "NEWS_COUNT" => "3",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "ACTIVE_FROM",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "",
        "FIELD_CODE" => array(0=>"",1=>"",),
        "PROPERTY_CODE" => array(0=>"",1=>"",),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "250",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "Y",
        "PAGER_TEMPLATE" => ".default",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "SET_BROWSER_TITLE" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y"
    )
);*/?>
	<p>
	</p>
 <br>
 <br>
	 <? endif;?> <br>
</div>
 <br>